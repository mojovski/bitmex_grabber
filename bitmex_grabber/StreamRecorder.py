"""
records incoming data formatted as dict or json
"""

import os
import sys
sys.path.append("./")

import zlib
import datetime
import pickle
import logging
from threading import Thread, Lock
import time
from shutil import copyfile
import json
import numpy
from datetime import date, datetime
from bitmex_grabber.MqttPublisher import MqttPublisher
import binascii
import gzip


def str2hex(s):
    return binascii.hexlify(s)


def hex2str(h):
    return binascii.unhexlify(h)


class StreamRecorder:
    def __init__(self, db_file_path, read_only, exchange_connector=None, 
        use_compression=True, compression_level=5, publish=True):
        self.file=None
        self.publish=publish
        self.mqtt=MqttPublisher("recorded_stream")
        self.compress=use_compression
        self.compression_level=compression_level
        self.db_file_path=db_file_path
        self.exchange_connector=exchange_connector
        self.messages=[]
        self.data_mutex=Lock()
        
        print("read only: "+str(read_only))
        try:
            if not read_only:
                if self.compress:
                    self.file = gzip.open(self.db_file_path, mode="wt")
                else:
                    self.file = open(self.db_file_path, "w")
            else:
                if self.compress:
                    self.file = gzip.open(self.db_file_path, mode="rt")
                else:
                    self.file = open(self.db_file_path, "r")

        except FileNotFoundError:
            raise Exception("Could not open the db file: "+str(db_file_path))

        if exchange_connector!=None:
            self.exchange_connector.addOnQuoteHandler(self.onMessage)
            self.exchange_connector.addOnTradeBinHandler(self.onMessage)
            self.exchange_connector.addOnBookHandler(self.onMessage)
            self.exchange_connector.addOnTradeHandler(self.onMessage)
        else:
            logging.warn("No exchange connector provided. Is this intended?")

        if not read_only:
            self.save_thr=Thread(target=self.save, daemon=True)
            self.save_thr.start()

    def json_serial(self, obj):
        """JSON serializer for objects not serializable by default json code"""
        if hasattr(obj, 'asDict'): #if "asDict" in val: #type(val)==type({'a':2}):
            dc=obj.asDict()
            out=json.dumps(dc, default=self.json_serial)
            return out
        
        if isinstance(obj, (datetime, date)):
            return obj.isoformat()
        
        if isinstance(obj, numpy.integer):
            return int(obj)
        elif isinstance(obj, numpy.floating):
            return float(obj)
        elif isinstance(obj, numpy.ndarray):
            return obj.tolist()

        raise TypeError("Type %s not serializable" % type(obj))

    def read(self, n):
        """
        n: n lines
        """
        out=[]
        for i in range(n):
            line=self.file.readline()
            if not line:
                break
            #print(line)
            obj=json.loads(line)
            out.append(obj)
        return out


    def __del__(self):
        if self.file!=None:
            self.file.close()
    
    def save(self):
        while True:
            time.sleep(10)
            logging.info("Writing Streamrecorder, "+str(len(self.messages))+" messages")
            self.data_mutex.acquire()
            for mi in self.messages:
                mi_str=json.dumps(mi, default=self.json_serial)
                self.file.write(mi_str+'\n') #this way, we can read line by line 
            self.messages=[]
            self.data_mutex.release()

    def onMessage(self, msg):
        """
        Receives an array of tickers along with timestamps.
        So the values dont arrive in real time here, but accumullated
        """
        #print(msg)
        self.data_mutex.acquire()
        obj = {'timestamp': datetime.now(), 'data': msg}
        self.messages.append(obj)
        self.data_mutex.release()

        if self.publish:
            #print("publishing obj under "+str(self.mqtt.topicname))
            self.mqtt.send_obj(obj)


def testStore(key, secret):
    from BitmexSocketConnector import BitmexWebsocketConnector
    out_file="/tmp/bitmex_grab.db"
    read_only=False
    bws=BitmexWebsocketConnector(key, secret)
    db=StreamRecorder(out_file, read_only, bws, use_compression=True)

    bws.run(symbol='XBTUSD', channels=[BitmexWebsocketConnector.Interval("tradeBin1m"),
         BitmexWebsocketConnector.Interval("tradeBin5m"),
         BitmexWebsocketConnector.Interval("quote"),
         BitmexWebsocketConnector.Interval("orderBook10")], max_reconnects=1)


def testRead():
    """
    tests wether reading the stored file works
    """
    filepath="/tmp/bitmex_grab.db"
    db = StreamRecorder(filepath, read_only=True)
    data=db.read(n=2)
    print(data)


if __name__ == "__main__":
    import sys
    key=sys.argv[0]
    secret=sys.argv[1]
    #testStore(key, secret)
    testRead()
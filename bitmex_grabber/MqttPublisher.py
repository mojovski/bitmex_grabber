import sys
import asyncio
import logging
import functools
import time

from datetime import date, datetime
import paho.mqtt.publish as publish
import paho.mqtt.client as mqtt
import json
import numpy 



class DateTimeEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()

        if isinstance(obj, numpy.integer):
            return int(obj)
        elif isinstance(obj, numpy.floating):
            return float(obj)
        elif isinstance(obj, numpy.ndarray):
            return obj.tolist()

        return json.JSONEncoder.default(self, o)

class MqttPublisher:
    def __init__(self, topicname="general", host="localhost"):
        self.topicname=topicname
        
        self.connected=False
        self.host=host
        self.callbacks={}

        self.connect()

    def on_disconnect(self, client, userdata, rc):
        self.connected=False

    def on_connect(self, client, userdata, flags, rc):
        self.connected=True

    def on_message(self, client, obj, msg):
        #handle the message mst.topic ...
        if msg.topic in self.callbacks:
            self.callbacks[msg.topic](msg.payload)



    def connect(self):
        self.client= mqtt.Client()
        self.client.on_disconnect=self.on_disconnect
        self.client.on_connect = self.on_connect
        self.client.connect(self.host, keepalive=160)


    def send_str(self, str_message):
        if self.connected==False:
            self.connect()
        self.client.publish(self.topicname, str.encode(str_message))

    def send_obj(self, msg_obj, topicname=None):
        try:
            if self.connected == False:
                self.connect()
        except Exception as e:
            logging.error(e)

        if topicname==None:
            topicname=self.topicname

        str_message=json.dumps(msg_obj, default=self.json_serial)
        try:
            self.client.publish(topicname, str.encode(str_message))
        except Exception as e:
            logging.error(e)
            try:
                self.connect()
            except Exception as e:
                logging.error("Failed to reconnect: "+str(e))

    def disconnect(self):
        self.client.disconnect()

    def json_serial(self, obj):
        """JSON serializer for objects not serializable by default json code"""

        if isinstance(obj, (datetime, date)):
            return obj.isoformat()
        
        if isinstance(obj, numpy.integer):
            return int(obj)
        elif isinstance(obj, numpy.floating):
            return float(obj)
        elif isinstance(obj, numpy.ndarray):
            return obj.tolist()

        raise TypeError("Type %s not serializable" % type(obj))


def testModule():
    m=MqttPublisher()
    m.connect()
    m.send('{"a": "hallo", "b":234}')
    time.sleep(1)
    m.disconnect()

if __name__ == '__main__':
    testModule()

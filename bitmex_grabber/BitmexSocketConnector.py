import websocket
import os
os.environ['BITMEX_API_KEY']="eqhxzQEQNLy3UzSJt1pJkjRq"
os.environ['BITMEX_API_SECRET']="Lf7jp4Nm33n55Lij2vx_b1hAu_3V5N02BkNJQjSzmEY6H6mm"

from bitmex_websocket import Instrument
from bitmex_websocket.constants import InstrumentChannels
from bitmex_websocket.constants import SecureInstrumentChannels
import sys
sys.path.append("./")


import logging
from time import sleep
import sys
from threading import Thread
import datetime
import dateutil.parser
import re
import pytz
local_timezone=pytz.timezone('Europe/Berlin')

class BitmexWebsocketConnector:

    #------------------------------------------------
    # internal interval class, provding .name() method
    #------------------------------------------------
    class Interval:
        def __init__(self, iname):
            self.name=iname
            """
            Possible names see:
            https://www.bitmex.com/app/wsAPI#Subscriptions
            for trades:
            tradeBin5m
            """

            
        def name(self):
            return self.name
    #-----------------------------------------------------------------
    #BitmexSocketConnector
    #-----------------------------------------------------------------

    def __init__(self, key, secret):
        #environ is used by the underlying bitmex websocket framework...
        os.environ['BITMEX_API_KEY']=key
        os.environ['BITMEX_API_SECRET']=secret

        self.log=self.setup_logger()

        #define on-Events
        self.onQuoteMsgHandlers=[]
        self.onTradeMsgHandlers=[]
        self.onBookMsgHandlers=[]
        self.onTradeBinMsgHandlers=[]
        self.onOrderMsgHandlers=[]
        self.onPositionMsgHandlers=[]

        #may use datetime instead of timewall, since it observes the socket connection in real time
        self.last_update_time=datetime.datetime.now()
        self.instrument=None

    """
    you can add multiple handles to one event type, eg quote(=Ticker)
    """
    def addOnQuoteHandler(self, h):
        self.onQuoteMsgHandlers.append(h)

    def addOnTradeHandler(self, h):
        self.onTradeMsgHandlers.append(h)

    def addOnBookHandler(self, h):
        self.onBookMsgHandlers.append(h)

    def addOnTradeBinHandler(self, h):
        self.onTradeBinMsgHandlers.append(h)

    def addOnOrderHandler(self, h):
        self.onOrderMsgHandlers.append(h)

    def addOnPositionHandler(self, h):
        self.onPositionMsgHandler.append(h)


    def _onMessage(self, msg):
        #logging.info(msg)
        try:
            if not 'table' in msg:
                #this is a bug in all recordingsin pt, rv3lX with X<20
                return
                #logging.info("table not in msg: "+str(msg))
                # if "side" in msg:
                #     #seems like a broken tade message?
                #     msg2={'table':"trade", 'data':msg}
                #     self.onTrade(msg2)
            if msg['table']=='trade':
                for hi in self.onTradeMsgHandlers:
                    hi(msg)
            if msg['table']=="orderBookL2":
                for hi in self.onBookMsgHandlers:
                    hi(msg)
            if msg['table']=="orderBookL2_25":
                for hi in self.onBookMsgHandlers:
                    hi(msg)
            if msg['table']=="orderBook10":
                for hi in self.onBookMsgHandlers:
                    hi(msg)
            if msg['table']=='quote':
                for hi in self.onQuoteMsgHandlers:
                    hi(msg)
            if 'tradeBin' in msg['table']:
                for hi in self.onTradeBinMsgHandlers:
                    hi(msg)
            if msg['table']=="position":
                for hi in self.onPositionMsgHandlers:
                    hi(msg)
            if msg['table']=="order":
                for hi in self.onOrderMsgHandlers:
                    hi(msg)
        except Exception as e:
            logging.error(msg)
            logging.error(e)
            import traceback
            s=traceback.format_exc()
            logging.error(s)
            raise e

        #logging.info("msg processed")
    def watchdog(self):
        if self.last_update_time < datetime.datetime.now():
            self.restart_connection()
    
    def run(self, symbol='XBTUSD', channels=[InstrumentChannels.quote], max_reconnects=1):
        """
        Provide channels. 
        e.g. 
        channels = [
                InstrumentChannels.quote,
                InstrumentChannels.trade,
                InstrumentChannels.orderBookL2
                InstrumentChannels.tradeBin5m
            ]
        """

        #websocket.enableTrace(True)
        """
        channels = [
            InstrumentChannels.quote,
            InstrumentChannels.trade,
            InstrumentChannels.orderBookL2
        ]
        """
        self.instrument = Instrument(symbol=symbol,
                        channels=channels, should_auth=True)
        self.instrument.on('action', self._onMessage)

        #setup a watchdog to reconnect if no data comes in for longer than 5 secs
        #t = Thread(target=watchdog)
        #t.start()

        c=0
        while c<max_reconnects:
            try:
                c+=1
                res=self.instrument.run_forever(ping_interval=1, ping_timeout=5)
                logging.info("Websocket returned with res="+str(res))
                logging.info("Was there a timeout? Try to reconnect...")
            except Exception as e:
                logging.error("Websocket connection cause an error: "+str(e))
                import traceback
                s=traceback.format_exc()
                logging.error(s)
        logging.info("Bitmex socket streamer is done!")

    def tear_down(self):
        """
        shuts down the websocket
        """
        logging.info("trying to call self.instrument.teardown()")
        self.instrument.teardown()

    def setup_logger(self):
        # Prints logger info to terminal
        logger = logging.getLogger()
        logger.setLevel(logging.INFO)  # Change this to DEBUG if you want a lot more info
        ch = logging.StreamHandler()
        # create formatter
        fmt="%(levelname)s: %(asctime)s.%(msecs)03d - [%(filename)s:%(lineno)s:%(funcName)s ] %(message)s"
        formatter = logging.Formatter(fmt)#"%(asctime)s - %(name)s - %(levelname)s - %(message)s")
        # add formatter to ch
        ch.setFormatter(formatter)
        logger.addHandler(ch)
        return logger



if __name__ == "__main__":

    bws=BitmexWebsocketConnector()
    bws.run([BitmexWebsocketConnector.Interval("quote")])
    #bws.run([BitmexWebsocketConnector.Interval("tradeBin1m")])
    #bws.run([SecureInstrumentChannels.position, SecureInstrumentChannels.order])

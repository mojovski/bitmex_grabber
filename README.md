# Bitmex Grabber

This project helps to grab live stream data from Bitmex and to store it in a compressed file (`*.db`).

## Features

### Compression

It basically stores pure json strings with gzip compression. Compared to raw test storage (use `StreamRecorder(use_compression=False`) the compressed one stores ca. 50MB per day compared to 1GB.

So if you plan to run the grabber on a server, this feature will save you a lot of storage-costs.

### Distributed Processing in Parallel 

Another fancy feature of this grabber, is that it republishes all the received bitmex data as mqtt messages.
This enables one to record (and later replay) a data set and to run a distributed process, which performs back-testing or real trading. 



## Usage without Docker

Install the requirements:

```sh
python3 -m venv venv 
source venv/bin/activate
pip install git+https://github.com/mojovski/bitmex-websocket.git
pip install paho-mqtt numpy dateutils requests 
```

Run a test:
```sh
source venv/bin/activate
cd bitmex_grabber
python StreamRecorder.py bitmexkey bitmexsecret
```


## Usage with Docker and Docker-Compose

`cd docker`

Build:
```sh

docker-compose -f record.yaml build
```

and run:

```sh
#cp example_config.eng .env
nano .env #set our kyes for bitmex
source .env
docker-compose -f record.yaml up -d
```

The file should be stored in `recorded_data` directory.






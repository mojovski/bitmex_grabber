import logging
import sys
sys.path.append("./")
sys.path.append("../")
import argparse
from bitmex_grabber.BitmexSocketConnector import BitmexWebsocketConnector
from bitmex_grabber import StreamRecorder
import os

parser = argparse.ArgumentParser()
parser.add_argument("--out_file", metavar="", dest="out_file", default="./recording.db", help="The directory where to store the recorded file")
parser.add_argument("--compression_level", type=int, help="Compresion level [1, 9]", default=5)
parser.add_argument('--compress', dest='compress', default=True, action='store_true', help="use zlib compression")

args = parser.parse_args()



received_out_file=args.out_file #"/tmp/bitmex_db.jsons"

#make sure that no file is ever overwritten
used_out_file=received_out_file
n=1
while os.path.isfile(used_out_file):
	used_out_file=received_out_file.split(".")[0]+"_"+str(n)+".db"
	n+=1

read_only=False
#assume the key and secret are stored in the envs:
if not 'BITMEX_API_KEY' in os.environ:
	raise Exception("You need to set bitmex key and secret to the env: BITMEX_API_KEY and BITMEX_API_SECRET")

key=os.environ['BITMEX_API_KEY']
secret=os.environ['BITMEX_API_SECRET']

bws=BitmexWebsocketConnector(key, secret)
db=StreamRecorder.StreamRecorder(used_out_file, read_only, bws,
	use_compression=args.compress, 
	compression_level=args.compression_level)
bws.run(channels=[BitmexWebsocketConnector.Interval("tradeBin1m"),
	BitmexWebsocketConnector.Interval("tradeBin5m"),
	BitmexWebsocketConnector.Interval("quote"),
	BitmexWebsocketConnector.Interval("orderBook10")], max_reconnects=100)